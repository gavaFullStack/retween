// chrome api
export const STORAGE_SYNC = chrome.storage.sync;

// config page
export const CONFIG_PAGE_TITLE = "Retween Configuration";
export const CONFIG_PAGE_RELATIVE_PATH = "html/config.html";
export const CONFIG_DEFAULT_IP = "127.0.0.1";
export const CONFIG_DEFAULT_PORT = 80;

// storage keys
export const MODE = "mode";
export const ENABLE_PROXY = "enableProxy";
export const AUTO_RELOAD_PAGE = "autoReloadPage";
export const DATA_LIST = "dataList";
export const TAB_PROXY_MAP = "tabProxyMap";


// others
export const SYSTEM_LOGO_PATH = "/img/system-logo64x64.png";
export const FIXED_LOGO_PATH = "/img/fixed-logo64x64.png";
