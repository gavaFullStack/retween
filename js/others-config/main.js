import { $ } from "../utils.js";
import { STORAGE_SYNC, AUTO_RELOAD_PAGE } from "../constants.js";

const reloadPageInput = $("#reload-page");

reloadPageInput.onchange = () => {
	STORAGE_SYNC.set({ autoReloadPage: reloadPageInput.checked });
};

init();

async function init() {
	const { autoReloadPage } = await STORAGE_SYNC.get(AUTO_RELOAD_PAGE);
	// 更改模式时自动刷新当前页面
	reloadPageInput.checked = autoReloadPage;
}
