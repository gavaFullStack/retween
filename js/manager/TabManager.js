import { STORAGE_SYNC, TAB_PROXY_MAP } from "../constants.js";

class TabManager {
	async put(tabId, proxyId) {
		const { tabProxyMap } = await STORAGE_SYNC.get(TAB_PROXY_MAP);
		if (!tabProxyMap) {
			tabProxyMap = {};
		}
		tabProxyMap[tabId] = proxyId;
		STORAGE_SYNC.set({ tabProxyMap });
	}

	async get(tabId) {
		const { tabProxyMap } = await STORAGE_SYNC.get(TAB_PROXY_MAP);
		return tabProxyMap[tabId];
	}
}

const tabManager = new TabManager();

export default tabManager;
