import { $, $$, toggleEle } from "../utils.js";

const mainSection = $("#main-section");
const popupWrapper = $("#popup-wrapper");
const tabWrap = $("#tab-wrap");
const tabs = $$("#tab-wrap li");
const selectedClassName = "selected";
const headerBg = $(".bg");

const showInsertWindowBtn = $("#show-insert-window-btn");

tabs.forEach((tab, index) => {
	const page = mainSection.children[index];
	const marginLeft = index * 20;
	if (index > 0) {
		page.style.marginLeft = `${marginLeft}px`;
	}

	tab.addEventListener("click", () => {
		const curClassList = tab.classList;

		if (curClassList.contains(selectedClassName)) {
			return;
		}

		if (index === 0) {
			showInsertWindowBtn.style.display = "block";
		} else {
			showInsertWindowBtn.style.display = "none";
		}

		const oldSelected = tabWrap.querySelector(`.${selectedClassName}`);
		oldSelected.classList.remove(selectedClassName);

		headerBg.style.left = `${tab.offsetLeft}px`;
		mainSection.style.transform = `translateX(-${
			index * 810 + marginLeft
		}px)`;
		curClassList.add(selectedClassName);
	});
});

showInsertWindowBtn.onclick = () => {
	toggleEle(popupWrapper);
};
