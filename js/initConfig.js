import {
	STORAGE_SYNC,
	MODE,
	AUTO_RELOAD_PAGE,
	DATA_LIST,
} from "./constants.js";

const { SYSTEM } = chrome.proxy.Mode;

// 预加载配置资源
export default async () => {
	let { mode, autoReloadPage, dataList } = await STORAGE_SYNC.get([
		MODE,
		AUTO_RELOAD_PAGE,
		DATA_LIST,
	]);

	// 默认是系统代理
	if (!mode) {
		mode = SYSTEM;
		STORAGE_SYNC.set({ mode });
	}

	// 自动刷新页面
	if (autoReloadPage === undefined) {
		autoReloadPage = true;
		STORAGE_SYNC.set({ autoReloadPage });
	}

	// 代理配置数据容器
	if (!dataList) {
		dataList = [];
		STORAGE_SYNC.set({ dataList });
	}
};
