![输入图片说明](img/full-logo.png)

# 介绍
<p>Retween是一个Chrome浏览器网络代理插件。</p>
<p>Retween不打算上架到Chrome应用商店。</p>
<p>Retween基于Manifest V3开发，使用本插件时请确认Chrome浏览器版本是88及以上。</p>

# 效果
![输入图片说明](img/config-page.png)
![输入图片说明](img/system-option.png)
![输入图片说明](img/fixed-option.png)

# 文档
<p>用户文档：<a href="https://gitee.com/gavaFullStack/retween/wikis/UserDocument">https://gitee.com/gavaFullStack/retween/wikis/UserDocument</a></p>
<p>版本文档：<a href="https://gitee.com/gavaFullStack/retween/releases">https://gitee.com/gavaFullStack/retween/releases</a></p>

# 交流
<p>QQ交流群：539699385</p>